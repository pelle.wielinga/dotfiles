(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Bootstrap use-package command used for getting packages ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(setq use-package-always-ensure t)

;;;;;;;;;;;
;; Theme ;;
;;;;;;;;;;;

(use-package zenburn-theme
  :config (load-theme 'zenburn t))

;;;;;;;;;;;;;;;;;;;;;;
;; Generic packages ;;
;;;;;;;;;;;;;;;;;;;;;;

(use-package better-defaults)

(use-package diminish)

;;;;;;;;;;;;;;;;;;;;;;
;; Project packages ;;
;;;;;;;;;;;;;;;;;;;;;;

(use-package projectile
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))

(use-package editorconfig
  :diminish editorconfig-mode
  :config (editorconfig-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;
;; Language specific ;;
;;;;;;;;;;;;;;;;;;;;;;;

;; Clojure

; Adds interacive clojure support
(use-package cider)

; Adds lots of refactor tools for clojure
(use-package clj-refactor)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic configuration ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;

; This makes any user changes to the config end up in the specified file. This
; prevents automatic changes to this carefully prepared emacs config. The file
; is not loaded. All config changes should be made to init.el instead.
(setq custom-file "~/.emacs.d/custom.el")

; Do not show the default startup screen. Instead open a scratch file.
(setq inhibit-startup-screen t)

; Prevent scrolling half a page when the cursor reaches the bottom. Scrolling
; just one line is expected behaviour for modern editors.
(setq scroll-conservatively 101)

; Enable line numbers
(setq column-number-mode t)

; Set the curosr to a vertical line
(setq-default cursor-type 'bar)

(setq tab-width 4)

; Keybindings for increasing and decreasing font size
(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C-<kp-add>") 'text-scale-increase)

(define-key global-map (kbd "C--") 'text-scale-decrease)
(define-key global-map (kbd "C-<kp-subtract>") 'text-scale-decrease)

; Make dired watch for changes on the file system apply them immediately
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; Autocomplete
;(install-package 'company)
;
;(global-set-key (kbd "TAB") #'company-indent-or-complete-common)
;(setq company-tooltip-align-annotations t)
;
;; -------------
;;   Languages
;; -------------
;
;; Clojure
;(install-package 'clojure-mode)
;(install-package 'clojure-test-mode)
;(install-package 'cider)
;
;; PHP
;(install-package 'php-mode)
;(require 'php-mode)
;
;; Haskell
;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
;
;; Rust
;(install-package 'rust-mode)
;(install-package 'racer)
;
;(setq racer-cmd "~/.cargo/bin/racer")
;(setq racer-rust-src-path "~/dev/3rdparty/rust/src")
;
;(add-hook 'rust-mode-hook #'racer-mode)
;(add-hook 'racer-mode-hook #'eldoc-mode)
;(add-hook 'racer-mode-hook #'company-mode)
;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;(add-to-list 'load-path "~/.emacs.d/themes")
;
;(setq backup-by-copying t)
;
;; Always save buffers instead of creating a # backup.
;(defun save-all ()
;  (interactive)
;  (save-excursion
;    (dolist (buf (buffer-list))
;      (if (and (buffer-file-name) (buffer-modified-p))
;        (basic-save-buffer)))))
;
;(setq auto-save-timeout 5)
;(setq auto-save-default nil)
;
;(add-hook 'auto-save-hook 'save-all)
;
;(defun to-char-forward (key)
;  (interactive)
;  (while (not (looking-at key)) (forward-char 1))
;  (forward-char 1))
;
;(defun to-char-backward (key)
;  (interactive)
;  (backward-char 2)
;  (while (not (looking-at key)) (backward-char 1))
;  (forward-char 1))
;
;(define-key global-map (kbd "C-M-(") (lambda () (interactive) (to-char-backward "(")))
;(define-key global-map (kbd "C-M-)") (lambda () (interactive) (to-char-forward "(")))
;
