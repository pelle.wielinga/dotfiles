#!/bin/bash

export DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# Note that using the install script causes these commands to be set after
# loading the default ~/.bashrc. If this file defines HISTSIZE and HISTFILESIZE
# to be lower, linux will immediately start trimming. Remove these values from
# your ~/.bashrc.
HISTSIZE=100000
HISTFILESIZE=200000

# Disable C-s and C-q for pausing and resuming programs. It also enables C-s
# for forward-i-search in the terminal.
stty -ixon

source "$DOTFILES_DIR/bash/ps.sh"
source "$DOTFILES_DIR/bash/aliases.sh"
source "$DOTFILES_DIR/bash/exports.sh"
