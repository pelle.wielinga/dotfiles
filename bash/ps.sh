#!/bin/bash

source "$DOTFILES_DIR/bash/color.sh"

GIT_PS1_SHOWDIRTYSTATE="true"
GIT_PS1_SHOWSTASHSTATE="true"
GIT_PS1_SHOWUNTRACKEDFILES="true"

__PW_PS1_CONNECT=""
__PW_PS1_CONNECT_OPEN=""

push_to_ps() {
  local text=$1
  local background=$2

  if [ -z "$__PW_PS1_CURRENT_BACKGROUND" ]; then
    __PW_PS1_CURRENT_BACKGROUND=$BLACK
  fi

  if [ -n "$PS1" ]; then
    if [ -z "$background" ] || [ "$background" == "$__PW_PS1_CURRENT_BACKGROUND" ]; then
      PS1="$PS1$__PW_PS1_CONNECT_OPEN"
    else
      PS1="$PS1$(color $__PW_PS1_CURRENT_BACKGROUND $background)$__PW_PS1_CONNECT$(color $BLACK $background)"
      __PW_PS1_CURRENT_BACKGROUND=$background
    fi
  elif [ -n "$background" ]; then
    PS1=$(color $BLACK $background)
    __PW_PS1_CURRENT_BACKGROUND=$background
  fi

  PS1="$PS1 $text "
}

finish_ps() {
  if [ -z "$__PW_PS1_CURRENT_BACKGROUND" ];then
    PS1="$PS1$__PW_PS1_CONNECT_OPEN "
  else
    PS1="$PS1$(color $__PW_PS1_CURRENT_BACKGROUND)$__PW_PS1_CONNECT $NC"
  fi
}

__prompt_command() {
  local EXIT="$?"

  echo -ne "\033]0;Terminal: ${PWD/#$HOME/\~}\007"

  PS1=""

  if [ $EXIT != 0 ]; then
    push_to_ps "exit $EXIT" $RED
  fi

  if [ -n "$(jobs)" ]; then
    push_to_ps "⚠ " $YELLOW
  fi

  if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
    push_to_ps "\h" $WHITE
  fi

  push_to_ps "\w" $BLUE

  local GIT="$(declare -F __git_ps1 > /dev/null && __git_ps1)"
  if [ -n "$GIT" ]; then
    PRETTY_GIT="$(echo "$GIT" | sed 's/[()]//g')"
    push_to_ps "$PRETTY_GIT" $GREEN
  fi

  finish_ps
}

PROMPT_COMMAND=__prompt_command
