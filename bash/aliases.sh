#!/bin/bash

alias vi='vim'
alias cdg='cd "$( git rev-parse --show-toplevel )"'
alias cdot='cd ~/dev/dotfiles'
alias xclip='xclip -selection clipboard'
