#!/bin/bash

BLACK=0
RED=1
GREEN=2
YELLOW=3
BLUE=4
MAGENTA=5
CYAN=6
WHITE=7

NC="\[\e[0m\]"

color() {
  local text_color=$1
  local background=$2
  local font_weight=$3

  if [ -z "$3" ]; then
    local font_weight=0
  fi

  if [ -z "$2" ]; then
    echo "\[\e[$font_weight;3${text_color}m\]"
  else
    echo "\[\e[$font_weight;4$background;3${text_color}m\]"
  fi
}
