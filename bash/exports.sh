#!/bin/bash

export BACKGROUND_COLOR=dark

function path_prepend {
  if [ -d "$1" ] ; then
    export PATH="$1:$PATH"
  fi
}

function path_append {
  if [ -d "$1" ] ; then
    export PATH="$PATH:$1"
  fi 
}

path_prepend "$HOME/.local/bin"
path_prepend "$HOME/.dotnet/tools"
path_prepend "$HOME/bin"
path_prepend "$DOTFILES_DIR/bin"

if [ -d "$HOME/tools" ] ; then
  for tool in $(ls "$HOME/tools")
  do
    path_append "$HOME/tools/$tool/bin"
    path_append "$HOME/tools/$tool/sbin"
  done
fi

path_append "$HOME/go/bin"
path_append "$HOME/tools/helm"
path_append "$HOME/tools/k9s"
path_append "$HOME/tools/lein"
path_append "$HOME/.local/bin"

if [ -d "$HOME/tools/android" ] ; then
  export ANDROID_HOME="$HOME/tools/android"
fi

if [ -d "$HOME/tools/java" ] ; then
  export JAVA_HOME="$HOME/tools/java"

  # Add java in front of path to make sure it comes before the default java
  path_prepend "$HOME/tools/java/bin"
fi

export KUBECONFIG="$HOME/.kube/config"

PATH=$(dedup-path $PATH)
