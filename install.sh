#!/bin/bash

DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DOTFILES_DIR/bash/color.sh"

function safelink {
  FILE="$DOTFILES_DIR/$1"
  LINK=$2

  printf "%-30s ->  %-45s " "$LINK" "$FILE"

  if [ -L "$LINK" ]; then
    # There is a link already
    echo -e "$(color $YELLOW)REPLACED EXISTING LINK$NC"
    rm "$LINK"
    ln -s "$FILE" "$LINK"

  elif [ -a "$LINK" ]; then
    # There is some other file or directory
    echo -e "$(color $RED)FILE EXISTS$NC"

  else
    ln -s "$FILE" "$LINK"
    echo -e "$(color $GREEN)DONE$NC"
  fi
}

function addSource {
  TARGET=$DOTFILES_DIR/$1
  FILE=$2

  COMMENT_LINE="Line added by Pelle's dotfiles"
  
  printf "%-30s ->  %-45s " "$FILE" "$TARGET"
  if (cat "$FILE" | grep -q "# $COMMENT_LINE") ; then
    echo -e "$(color $YELLOW)FILE ALREADY SOURCED$NC"
  else
    echo -e "\nsource \"$TARGET\" # $COMMENT_LINE" >> "$FILE"
    echo -e "$(color $GREEN)ADDED SOURCE$NC"
  fi
}

safelink "vim/.vimrc" "$HOME/.vimrc"
safelink "vim/.vim" "$HOME/.vim"
#safelink "vim/.vimperatorrc" "$HOME/.vimperatorrc"
safelink "git/.gitconfig" "$HOME/.gitconfig"
safelink "emacs" "$HOME/.emacs.d"
#safelink ".screenrc" "$HOME/.screenrc"
#safelink "i3config" "$HOME/.config/i3/config"
#safelink "polybar" "$HOME/.config/polybar"
safelink "Xresources" "$HOME/.Xresources"

safelink "zsh/.p10k.zsh" "$HOME/.p10k.zsh"

addSource "bash/bash_rc.sh" "$HOME/.bashrc"
addSource "bash/bash_profile.sh" "$HOME/.bash_profile"
addSource "zsh/.zshrc" "$HOME/.zshrc"
