# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export DOTFILES_DIR="$HOME/dev/dotfiles"

# Lines configured by zsh-newuser-install
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.histfile
setopt extendedglob notify
unsetopt autocd beep nomatch

# vi keybindings with insert and command modes.
# Mostly pretty cool but also missing some really
# standard stuff.
bindkey -v

# delete in normal and command mode respectively
bindkey '^[[3~' delete-char
bindkey -a '^[[3~' delete-char

# ctrl-a and ctrl-e in insert mode
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line

# home and end in insert mode
bindkey '^[[H' beginning-of-line
bindkey '^[[F' end-of-line

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Color auto is only required on linux and doesn't work on mac
if [[ "$OSTYPE" == darwin* ]]; then
  alias ls="ls -G"
else
  alias ls="ls --color=auto"
fi

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

source "$HOME/dev/3rdparty/powerlevel10k/powerlevel10k.zsh-theme"
source "$HOME/dev/dotfiles/bash/aliases.sh"
source "$HOME/dev/dotfiles/bash/exports.sh"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

