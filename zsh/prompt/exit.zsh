#!/usr/bin/env zsh

if [ "$DOT_ZSH_EXIT_CODE" != 0 ]; then
  section_text="exit $DOT_ZSH_EXIT_CODE"
fi
