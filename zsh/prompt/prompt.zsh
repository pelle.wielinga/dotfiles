#!/usr/bin/env zsh

SCRIPT_DIR="${0:a:h}"

DOT_ZSH_SECTIONS=()
DOT_ZSH_COLORS=()

function load_module() {
  local section="$1"
  local background="$2"

  if [ ! -f "$SCRIPT_DIR/$section.zsh" ]; then
    print "Cannot find section '$section' in ${0:a:h}"
    return
  fi

  local section_text

  source "$SCRIPT_DIR/$section.zsh"

  if [ "$section_text" != "" ]; then
    DOT_ZSH_SECTIONS+=("$section_text")
    DOT_ZSH_COLORS+=("$background")
  fi
}

function print_sections() {
  PROMPT="%F{black}"

  local current=""

  while [ ${#DOT_ZSH_SECTIONS[@]} != 0 ]; do
    local section="${DOT_ZSH_SECTIONS[1]}"
    local background="${DOT_ZSH_COLORS[1]}"

    if [ "$current" = "" ]; then
      PROMPT="$PROMPT%K{$background}"
    elif [ "$current" = "$background" ]; then
      PROMPT="${PROMPT}"
    else
      PROMPT="$PROMPT%F{$current}%K{$background}%F{black}"
    fi

    PROMPT="$PROMPT $section "

    current="$background"

    shift DOT_ZSH_SECTIONS
    shift DOT_ZSH_COLORS
  done

  if [ "$current" = "" ]; then
    PROMPT="${PROMPT}%f "
  else
    PROMPT="$PROMPT%F{$current}%k%f "
  fi
}

precmd() {
  local DOT_ZSH_EXIT_CODE="$?"

  vcs_info

  load_module "exit" "red"
  load_module "dir" "blue"
  load_module "git" "green"

  print_sections
}
